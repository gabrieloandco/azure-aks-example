variable "client_id " {
  type        = string
  description = "Client Id to be used"

}

variable "client_secret " {
  type = number
  description = "Client Secret"
}

variable "location " {
  type = string
  description = "Location"
  default= "eastus"
}


provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "aks_resource_group" {
  name     = "aks-resource-group"
  location = "eastus"
}

module "acr" {
    source  = "aztfmod/caf-container-registry/azurerm"
    version = "2.0.0"

    resource_group_name               = azurerm_resource_group.aks_resource_group.name
    name                              = "acr-example-registry"
    location                          = var.location
    tags                              = ""
    diagnostics_map                   = ""
    la_workspace_id                   = ""
}

module "network" {
  source              = "Azure/network/azurerm"
  resource_group_name = azurerm_resource_group.aks_resource_group.name
  address_space       = "10.0.0.0/16"
  subnet_prefixes     = ["10.0.1.0/24"]
  subnet_names        = ["subnet1"]
  depends_on          = [azurerm_resource_group.aks_resource_group]
}

data "azuread_group" "aks_cluster_admins" {
  name = "AKS-cluster-admins"
}

module "aks" {
  source                           = "Azure/aks/azurerm"
  resource_group_name              = azurerm_resource_group.aks_resource_group.name
  client_id                        = var.client_id
  client_secret                    = var.client_secret
  kubernetes_version               = "1.19.3"
  orchestrator_version             = "1.19.3"
  prefix                           = "prefix"
  cluster_name                     = "aks-example-cluster"
  network_plugin                   = "azure"
  vnet_subnet_id                   = module.network.vnet_subnets[0]
  os_disk_size_gb                  = 50
  sku_tier                         = "Paid" # defaults to Free
  enable_role_based_access_control = true
  rbac_aad_admin_group_object_ids  = [data.azuread_group.aks_cluster_admins.id]
  rbac_aad_managed                 = true
  private_cluster_enabled          = true # default value
  enable_http_application_routing  = true
  enable_azure_policy              = true
  enable_auto_scaling              = true
  agents_min_count                 = 1
  agents_max_count                 = 2
  agents_count                     = null 
  agents_max_pods                  = 100
  agents_pool_name                 = "exnodepool"
  agents_availability_zones        = ["1", "2"]
  agents_type                      = "VirtualMachineScaleSets"

  agents_labels = {
    "nodepool" : "defaultnodepool"
  }

  agents_tags = {
    "Agent" : "defaultnodepoolagent"
  }

  network_policy                 = "azure"
  net_profile_dns_service_ip     = "10.0.0.10"
  net_profile_docker_bridge_cidr = "170.10.0.1/16"
  net_profile_service_cidr       = "10.0.0.0/16"

  depends_on = [module.network]
}

provider "helm" {
  kubernetes {
    host                   = module.aks
    cluster_ca_certificate = base64decode(var.cluster_ca_cert)
    exec {
      api_version = "client.authentication.k8s.io/v1alpha1"
      args        = ["aks", "get-credentials", "--name", module.aks.cluster_name , "--resource-group",  azurerm_resource_group.aks_resource_group.name]
      command     = "az"
    }
  }
}


resource "helm_release" "nginx_ingress" {
  name       = "ingress-nginx"

  repository = "https://kubernetes.github.io/ingress-nginx/"
  chart      = "ingress-nginx"

  set {
    name  = "service.type"
    value = "ClusterIP"
  }
}




