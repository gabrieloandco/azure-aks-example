FROM nginx:1.21.0
ARG VERSION=1
COPY ./nginx.confv${VERSION} /etc/nginx/conf.d/default.conf
CMD ["/bin/bash", "-c", "nginx -g \"daemon off;\""]