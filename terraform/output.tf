output "registry_admin_username" {
  value = module.acr.admin_username 
}

output "registry_admin_password" {
  value = module.acr.admin_password 
}

output "registry_login_server" {
  value = module.acr.login_server
}

output "aks_kube_config_raw" {
  value = module.aks.kube_config_raw
}

